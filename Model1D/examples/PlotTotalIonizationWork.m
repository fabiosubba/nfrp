close all; clear; clc;
% This example shows how to plot the total ionization work for the first 86
% elements

load('ionization_potentials','ionization_potentials');
% Load the ionization potential database
% For element IE, ionization_potentials(IE,1:IE) stores the ionization
% potentials according to the following scheme:
% ionization_potentials(IE,1) is the the first ionization potential (from
% neutral status, to ion with charge 1)
% ionization_potentials(IE,2) is the the second ionization potential (from
% ion with charge 1, to ion with charge 2)
% ...

nele = size(ionization_potentials,1);
% Determine how many elements are available

work = NaN(nele,1);
for iele = 1:nele
    work(iele) = sum(ionization_potentials(iele,1:iele));
end
% For each element, determine the work needed to completely ionize it (in
% eV)

figure;
plot(work,'linewidth',2);
xlabel('Atomic number');
ylabel('Total ionization work (eV)');
set(gca,'fontsize',14);
