close all; clear; clc;
% This example is intended to show how to use the EvalRate function to
% produce ionization and recombination reaction rates for deuterium at
% different electron densities over a selected density range.

LOG10_TMIN = -2;
LOG10_TMAX = 5;
% Set the minimum and maximum desired temperatures (in eV). Note that
% log10(T) is actually used

load('ionization_rate','ionization_rate');
load('recombination_rate','recombination_rate');
% Load ionization and recombination rate

Nval = 100;
T = logspace(LOG10_TMIN,LOG10_TMAX,Nval);
% Set the temperature array. Make sure values are logarithmically spaced

density = [1e21 1e20 1e19 1e18 1e17 1e16 1e15];
ND = numel(density);
% Set the desired electron densities
n21 = 1e21;
n20 = 1e20;
n19 = 1e19;
n18 = 1e18;
n17 = 1e17;
n16 = 1e16;
n15 = 1e15;
% Set values for the density

rsa = NaN(Nval,ND);
rra = NaN(Nval,ND);
% Allocate space for ionization and recombination rates.
% rsa will store ionization rates for D^0, rra recombination rates for D+
% Each column corresponds to a different electron density
% Each row corresponds to a different electron temperature

rsa21 = NaN(Nval,1);
rsa20 = NaN(Nval,1);
rsa19 = NaN(Nval,1);
rsa18 = NaN(Nval,1);
rsa17 = NaN(Nval,1);
rsa16 = NaN(Nval,1);
rsa15 = NaN(Nval,1);
% Initialize ionization rates

rra21 = NaN(Nval,1);
rra20 = NaN(Nval,1);
rra19 = NaN(Nval,1);
rra18 = NaN(Nval,1);
rra17 = NaN(Nval,1);
rra16 = NaN(Nval,1);
rra15 = NaN(Nval,1);
% Initialize recombination rates

chemical_symbol = 'H';
for iv = 1:Nval
    
    ionization_stage = 0;
    for id = 1:ND
        rsa(iv,id) = EvalRate(density(id),T(iv),'H',...
            ionization_stage,ionization_rate);
    end
    % Populate ionization rates table (rsa)
    
    ionization_stage = 1;
    for id = 1:ND
        rra(iv,id) = EvalRate(density(id),T(iv),'H',...
            ionization_stage,recombination_rate);
    end
end
% Populate recombination rates table (rra)

figure;
hold on;
% Initialize plot

hp = NaN(ND,1);
for id = 1:ND
    hp(id) = plot(T,rsa(:,id),'linewidth',2);
end
for id = 1:ND
    plot(T,rra(:,id),'linewidth',2,...
        'color',get(hp(id),'color'),'linestyle','--');
end


xlabel('Temperature (eV)');
ylabel('Rates (m^3/s)');
title('Continuous: D^{0} ionization; dashed: D^{+} recombination');
set(gca,'xscale','log','yscale','log','fontsize',14);
% Set a nicely readable graphics

plot([1e-1 1e-1],get(gca,'ylim'),'linewidth',2,'linestyle','--','color','k');
plot([1e4 1e4],get(gca,'ylim'),'linewidth',2,'linestyle','--','color','k');
% Mark the extrema of the table values. Outer temperatures are extrapolated

lbls = cell(ND,1);
for id = 1:ND
    lbls{id} = sprintf('n_{e} = %8.1e m^{-3}',density(id));
end
% Define the legend labels 

legend(lbls,'location','south');
% Put the legend
