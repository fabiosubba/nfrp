close all; clear all; clc;

%% NFRP                     12/04/2022,Turin
%% User data

VV = 1600;      %[m^3]  volume
n_tot = 1e20;   %[m^-3] total density
n_e = 1e14;     %[m^-3] electron density
n_ion = n_e;    %[m^-3] ion density
n_0 = n_tot - n_ion; %[m^-3] neutral density
% n_0 = 1e14;
Tin = 10;       %[eV]   initial temperature
atomic_number = 1; %specify the number of the atomic elements that you want to use
Pot = 10*1e6;   %[W] external power
tau = 3;        %[s] time constant

%% Numerical part

load('ionization_potentials','ionization_potentials');
load('ionization_rate','ionization_rate'); 
load('recombination_rate','recombination_rate');
[ const ] = PhysicalConstants(  );

nele = size(atomic_number,1);
work = NaN(nele,1);
for iele = 1:nele
    work(iele) = sum(ionization_potentials(iele,1:iele));
end

E0 = 1.5*(n_0+n_ion+n_e)*Tin*const.KB*VV/const.KB_eV; %[J] initial energy

%% Setup variable for while cycle

time = 0; %[s]
dt = 0.5*1e-3/10; %[s]
ii = 1;

nn_evol = n_0;
ni_evol = n_ion;
T_evol = Tin;
E_evol = E0;
ne_evol = n_e;
TT = Tin;
ris_ion_evol = 0;
ris_rec_evol = 0;
 n_sum = n_ion+n_0;

%% While cycle

while  time(ii)<15
    
    ii = ii+1;

    ris_ion = EvalRate(n_e,TT,'H',0,ionization_rate);
    ris_rec = EvalRate(n_e,TT,'H',1,recombination_rate);
    ris_ion_evol(ii) = ris_ion;
    ris_rec_evol(ii) = ris_ion;

    k1 = dnplus_dt(n_0,n_e,ris_ion,ris_rec,n_ion);
    k2 = dnplus_dt(n_0,n_e,ris_ion,ris_rec,n_ion+.5*k1*dt);
    k3 = dnplus_dt(n_0,n_e,ris_ion,ris_rec,n_ion+.5*k2*dt);
    k4 = dnplus_dt(n_0,n_e,ris_ion,ris_rec,n_ion+k3*dt);
    n_ion = n_ion+((k1+2*k2+2*k3+k4)/6)*dt;
    ni_evol(ii) = n_ion;

%     k1 = dn0_dt(n_0,n_e,ris_ion,ris_rec,n_ion);
%     k2 = dn0_dt(n_0+.5*k1*dt,n_e,ris_ion,ris_rec,n_ion);
%     k3 = dn0_dt(n_0+.5*k2*dt,n_e,ris_ion,ris_rec,n_ion);
%     k4 = dn0_dt(n_0+k3*dt,n_e,ris_ion,ris_rec,n_ion);
%     n_0 = n_0+((k1+2*k2+2*k3+k4)/6)*dt;
%     nn_evol(ii) = n_0;
% 
    n_0 = n_tot - n_ion; 
    nn_evol(ii) = n_0;

    n_e = n_ion;
    ne_evol(ii) = n_e;
    n_sum(ii) = n_ion+n_0;
   

    k1 = cons_E(E0,tau,Pot);
    k2 = cons_E(E0+.5*k1*dt,tau,Pot);
    k3 = cons_E(E0+.5*k2*dt,tau,Pot);
    k4 = cons_E(E0+k3*dt,tau,Pot);
    E0 = E0+((k1+2*k2+2*k3+k4)/6)*dt;
%     En(ii) = E0; %[J]
 
    % remove the additional costs due to ionization
%     E0 = E0-work(nele)*n_ion*VV/const.KB_eV*const.KB; %[J]
    E_evol(ii) = E0;

    TT = E0/1.5/(n_0+n_ion+n_e)/VV/const.KB*const.KB_eV; %[eV]
    T_evol(ii) = TT;

    time(ii) = time(ii-1) + dt;

end

figure
semilogy(time,T_evol);
xlabel('Time [s]')
ylabel('T [eV]')
grid on
box on

figure
semilogy(time,E_evol)
xlabel('Time [s]')
ylabel('E [J]')
grid on
box on

figure
semilogy(time,ni_evol)
xlabel('Time [s]')
ylabel('ion density [m^-3]')
grid on
box on

figure
semilogy(time,ne_evol)
xlabel('Time [s]')
ylabel('electron density [m^3]')
grid on
box on

figure
plot(time,nn_evol)
xlabel('Time [s]')
ylabel('neutral density [m^-3]')
grid on
box on

figure
semilogy(time,ris_rec_evol)
xlabel('Time [s]')
ylabel('rec rate [m^-3s^-1]')
grid on
box on

figure
semilogy(time,ris_ion_evol)
xlabel('Time [s]')
ylabel('ion rate [m^-3s^-1]')
grid on
box on

figure
semilogy(time,n_sum)
xlabel('Time [s]')
ylabel(' [m^-3]')
grid on
box on