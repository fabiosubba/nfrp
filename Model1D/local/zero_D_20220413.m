close all; clear; clc;
%% NFRP ZERO-D MODEL                       28/03/2022,Turin
%% Datas
[user0,sys0]=memory;
%
% GEOMETRY
%
VV = 1600;       %[m^3] plasma volume

%
% PHYSICS
%
tau = 3;               %[s] Energy confinement time
Pot = 10*1e6;          %[W] Heating power
species = {'H'};       %Plasma species list
total_density = 1e20;  %[m^-3] total particle density

%
% INITIAL CONDITIONS
%
n_0 = 10^20;     %[m^3] neutral D atoms density (neither other species or ions considered at the moment)
T_0 = 300;       %[K] initial temperature
ne_0 = 1e12;     %[m^-3] initial electron density

%
% NUMERICS
%

dt = 2e-7;      %[s] time step
ntime = 100; % 500000;    %Number of required time steps
NDIAG =  50000; % Output array maximum size

%
% COMPUTATIONS
%
[ const ] = PhysicalConstants(  );
% Load physical constants

outdir = replace(replace(string(datetime('now')),' ','-'),':','');
[status, msg]=mkdir(outdir);
assert(isempty(msg),sprintf('Output directory creation failed with mesage %s',msg));

fout = fopen(fullfile(outdir,'outputlog.txt'),'w');
fprintf(fout,'%%  Out       start         end\n');
% Creates output directory and open log file

load('ionization_rate','ionization_rate'); 
load('recombination_rate','recombination_rate');
% Load relevant databases
tic;
plasma = initialize_plasma(species,ne_0,T_0,total_density,const);
E0 = plasma.energy_density;

%% Setup variable for while cycle



%% Evolve the plasma state
% tic;
% time = zeros(ntime+1,1);
% En = E0*ones(ntime+1,1);
% electron_density = plasma.electron_density*ones(ntime+1,1);
% total_particle_density = (sum(plasma.ion_density)+plasma.electron_density)*ones(ntime+1,1);
% temperature = 2*En(1)/(3*total_particle_density(1))*ones(ntime+1,1)/const.ev;

time = 0.0;
En = E0*ones(NDIAG,1);
electron_density = plasma.electron_density*ones(NDIAG,1);
total_particle_density = (sum(plasma.ion_density)+plasma.electron_density)*ones(NDIAG,1);
temperature = 2*En(1)/(3*total_particle_density)*ones(NDIAG,1)/const.ev;

% diagnostic = initialize_diagnostic(species,ntime,time,plasma,T_0);
diagnostic = initialize_diagnostic(species,NDIAG,time,plasma,T_0);

[user1,sys1]=memory;

iidiag = 1;
iout = 0;
timestart = 0.;
for ii = 1:ntime 
    
    iidiag = mod(iidiag,NDIAG)+1;
    % Increment the pointer to the diagnostic arrays. It loops cyclically
    % between 1 and NDIAG. Data need to be periodically damped to an output
    % file to avoid loosing data
    
    x = [plasma.ion_density; plasma.energy_density];
    % Build a vector variable to feed the Runge-Kutta solver
    
    [k1,diagnostic] = plasma_state_timeder(x,plasma,tau,Pot,VV,const,...
        ionization_rate,recombination_rate,diagnostic,iidiag);
    % Evaluate the time derivative of the state vector x. Note that
    % diagnostics describing sources/sinks involve contributions to dx/dt.
    % They are not associated with a single plasma state but a differential
    % interval dx. They should then be assigned here.

    dx = k1*dt;
    x = x+dx;
    % Evolve the plasma state. Explicit Euler is applied here
    
    
    plasma.ion_density = x(1:end-1);
    plasma.energy_density = x(end);
    plasma.electron_density = sum(plasma.ion_density .* plasma.charge_state);
    % Update the single plasma state. First set informations which are
    % just copies of the state vector elements x. Then set derived 
    % quantities
    
    time_old = time;
    time = time + dt; 
    % Increment the time value

    diagnostic.time(iidiag) = time;
    diagnostic.energy_density(iidiag) = plasma.energy_density;
    diagnostic.electron_density(iidiag) = plasma.electron_density;
    total_particle_density = sum(plasma.ion_density)+plasma.electron_density;
    diagnostic.ion_density(iidiag) = sum(plasma.ion_density);
    diagnostic.temperature(iidiag) = 2*plasma.energy_density/...
        (3*total_particle_density)/const.ev;
    % Update diagnostics for later post-processing
    
    if iidiag==NDIAG
        fprintf(fout,'%6d  %10.3e  %10.3e\n',iout,timestart,time);
        timestart = time;
        % Update the log file
        
        outfile = sprintf('time_diagnstics_%06d',iout);
        save(fullfile(outdir,outfile),'diagnostic');
        iout = iout+1;
    end
    % Save a bunch of diagnostic values and increments the counter
    
end
    if iidiag<NDIAG
        diagnostic.ion_density(:,iidiag+1:end) = NaN;
        diagnostic.electron_density(iidiag+1:end) = NaN;
        diagnostic.temperature(iidiag+1:end) = NaN;
        diagnostic.energy_density(iidiag+1:end) = NaN;
        diagnostic.time(iidiag+1:end) = NaN;
        diagnostic.ionizations(:,iidiag+1:end) = NaN;
        diagnostic.recombinations(:,iidiag+1:end) = NaN;
        diagnostic.ionization_power(:,iidiag+1:end) = NaN;
        diagnostic.surface_losses(iidiag+1:end) = NaN;
        diagnostic.total_ion_power(iidiag+1:end) = NaN;
        diagnostic.auxiliary_heating(iidiag+1:end) = NaN;
        % Cancel not used data

        fprintf(fout,'%6d  %10.3e  %10.3e\n',iout,timestart,time);
        outfile = sprintf('time_diagnstics_%06d',iout);
        save(fullfile(outdir,outfile),'diagnostic');
        % Output the last bunch of data;
    end
fclose(fout);
% Close the log file
toc;

[user2,sys2]=memory;

