close all; clear all; clc;
%% NFRP ZERO-D MODEL                       28/03/2022,Turin
%% Datas

VV = 1600; %[m^3] plasma's volume
n_0 = 10^20; %[m^3] neutral D atoms density (neither other species or ions considered at the moment)
tau = 3; %[s] time's constant
T_0 = 300; %[K] initial T
Pot = 10*1e6; %[W] entering power
dt = 1e-2; %[s] time step

% k_boltz = 8.6167*1e-5; %[eV/K] Boltzmann's constant
% qq = 1.6*1e-19; %[J/eV]
[ const ] = PhysicalConstants(  );
E0 = 1.5*n_0*T_0*const.KB*VV; %[J] initial energy

%% Setup variable for while cycle

time = 0; %[s]
ii = 1;
toll = 1;
err = 10*toll;
En = E0;

%% While cycle

while err(ii)>toll %the while cycle will perform until the error reaches the tollerance
    
    ii = ii+1;
    k1 = cons_E(E0,tau,Pot);
    k2 = cons_E(E0+.5*k1*dt,tau,Pot);
    k3 = cons_E(E0+.5*k2*dt,tau,Pot);
    k4 = cons_E(E0+k3*dt,tau,Pot);
    E0 = E0+((k1+2*k2+2*k3+k4)/6)*dt;
    En(ii) = E0;
    time(ii) = time(ii-1) + dt;
    err(ii) = abs(En(ii)-En(ii-1));

end

%% Post processing

figure
plot(time,En/1e6,'r','linewidth',4)
title('0D-MODEL')
xlabel('Time [s]')
ylabel('E [MJ]')
xlim([En(1)/1e6 En(end)/1e6])
box on
grid on

set(gca,'fontsize',20)
set(gcf, 'color', 'y')

%% Tau evaluation

%% Constant density
load('ionization_rate','ionization_rate'); 
load('recombination_rate','recombination_rate');
T_0D_constD = En./(const.ev*1.5*n_0*VV);
n_e = [1e4 1e8 1e12 1e16 0.99*1e20];
ris_ion_constD = NaN(length(T_0D_constD),length(n_e));
ris_rec_constD = NaN(length(T_0D_constD),length(n_e));
tau_ion_constD = NaN(length(T_0D_constD),length(n_e));
tau_rec_constD = NaN(length(T_0D_constD),length(n_e));


for jj = 1:length(T_0D_constD)

    for ii = 1:length(n_e)
    
        ris_ion_constD(jj,ii) = EvalRate(n_e(ii),T_0D_constD(jj),'H',0,ionization_rate);
        tau_ion_constD(jj,ii) = 1/(ris_ion_constD(jj,ii)*n_e(ii));
        
    end

    for ii = 1:length(n_e)
    
        ris_rec_constD(jj,ii) = EvalRate(n_e(ii),T_0D_constD(jj),'H',1,recombination_rate);
        n_0 = 1e20 - n_e(ii);
        tau_rec_constD(jj,ii) = 1/(ris_rec_constD(jj,ii)*n_e(ii)*n_e(ii)/n_0);
        
    end

end

figure
loglog(T_0D_constD,tau_ion_constD(:,1),'b','LineWidth',3)
hold on
loglog(T_0D_constD,tau_ion_constD(:,2),'m','LineWidth',3)
hold on
loglog(T_0D_constD,tau_ion_constD(:,3),'r','LineWidth',3)
hold on
loglog(T_0D_constD,tau_ion_constD(:,4),'k','LineWidth',3)
hold on
loglog(T_0D_constD,tau_ion_constD(:,5),'y','LineWidth',3)
hold on
xlabel('Temperature [eV]');
ylabel('Tau [s]');
title('Tau ionization constant density, Tau recombination constant density (dashed lines)')
% legend('1e4 [m^3]','1e8 [m^3]','1e12 [m^3]','1e16 [m^3]','0.991e20 [m^3]','1e4 [m^3]','1e8 [m^3]','1e12 [m^3]','1e16 [m^3]','0.991e20 [m^3]','location','best')

% figure
loglog(T_0D_constD,tau_rec_constD(:,1),'LineWidth',3,'linestyle','--','color','b')
hold on
loglog(T_0D_constD,tau_rec_constD(:,2),'LineWidth',3,'linestyle','--','color','g')
hold on
loglog(T_0D_constD,tau_rec_constD(:,3),'LineWidth',3,'linestyle','--','color','r')
hold on
loglog(T_0D_constD,tau_rec_constD(:,4),'LineWidth',3,'linestyle','--','color','k')
hold on
loglog(T_0D_constD,tau_rec_constD(:,5),'LineWidth',3,'linestyle','--','color','y')
% xlabel('Temperature [eV]');
% ylabel('Tau [s]');
% xlim([100 T_0D_constD(end)])
% title('Tau recombination constant density')
% legend('1e4 [m^3]','1e8 [m^3]','1e12 [m^3]','1e16 [m^3]','0.991e20 [m^3]','location','best')
legend('1e4 [m^3]','1e8 [m^3]','1e12 [m^3]','1e16 [m^3]','0.991e20 [m^3]','1e4 [m^3]','1e8 [m^3]','1e12 [m^3]','1e16 [m^3]','0.991e20 [m^3]','location','best')

figure
semilogy(T_0D_constD,ris_ion_constD(:,1),'b','LineWidth',3)
hold on
semilogy(T_0D_constD,ris_ion_constD(:,2),'g','LineWidth',3)
hold on
semilogy(T_0D_constD,ris_ion_constD(:,3),'r','LineWidth',3)
hold on
semilogy(T_0D_constD,ris_ion_constD(:,4),'k','LineWidth',3)
hold on
semilogy(T_0D_constD,ris_ion_constD(:,5),'y','LineWidth',3)
hold on
xlabel('Temperature [eV]');
ylabel('Rates [m^3/s]');
xlim([100 T_0D_constD(end)]) %I plotted in this range to make a better plot
title('Ionization rates constant density, Recombination rates constant density (dashed lines)')
% legend('1e4 [m^3]','1e8 [m^3]','1e12 [m^3]','1e16 [m^3]','0.991e20 [m^3]','1e4 [m^3]','1e8 [m^3]','1e12 [m^3]','1e16 [m^3]','0.991e20 [m^3]','location','best')

% figure
semilogy(T_0D_constD,ris_rec_constD(:,1),'LineWidth',3,'linestyle','--','color','b')
hold on
semilogy(T_0D_constD,ris_rec_constD(:,2),'LineWidth',3,'linestyle','--','color','g')
hold on
semilogy(T_0D_constD,ris_rec_constD(:,3),'LineWidth',3,'linestyle','--','color','r')
hold on
semilogy(T_0D_constD,ris_rec_constD(:,4),'LineWidth',3,'linestyle','--','color','k')
hold on
semilogy(T_0D_constD,ris_rec_constD(:,5),'LineWidth',3,'linestyle','--','color','y')
legend('1e4 [m^3]','1e8 [m^3]','1e12 [m^3]','1e16 [m^3]','0.991e20 [m^3]','1e4 [m^3]','1e8 [m^3]','1e12 [m^3]','1e16 [m^3]','0.991e20 [m^3]','location','best')

% xlabel('Temperature [eV]');
% ylabel('Rates [m^3/s]');
% xlim([100 T_0D_constD(end)])
% title('Recombination rates constant density')
% legend('1e4 [m^3]','1e8 [m^3]','1e12 [m^3]','1e16 [m^3]','0.991e20 [m^3]','location','best')

%% Constant temperature
T_0D_constT = [0.026 10 100 700];
n_e = linspace(1e4,0.99*1e20,3456)';
chemical_symbol = 'H';
ris_ion_constT = NaN(length(n_e),length(T_0D_constT));
ris_rec_constT = NaN(length(n_e),length(T_0D_constT));
tau_ion_constT = NaN(length(n_e),length(T_0D_constT));
tau_rec_constT = NaN(length(n_e),length(T_0D_constT));

for jj = 1:length(n_e)

    for ii = 1:length(T_0D_constT)
    
        ris_ion_constT(jj,ii) = EvalRate(n_e(jj),T_0D_constT(ii),'H',0,ionization_rate);
        tau_ion_constT(jj,ii) = 1/(ris_ion_constT(jj,ii)*n_e(jj,1));
        
    end

    for ii = 1:length(T_0D_constT)
    
        ris_rec_constT(jj,ii) = EvalRate(n_e(jj),T_0D_constT(ii),'H',1,recombination_rate);
        n_0  = 1e20 - n_e(jj,1);
        tau_rec_constT(jj,ii) = 1/(n_e(jj,1)*n_e(jj,1)*ris_rec_constT(jj,ii)/n_0);

    end

end

figure
loglog(n_e,tau_ion_constT(:,1),'b','LineWidth',3)
hold on
loglog(n_e,tau_ion_constT(:,2),'g','LineWidth',3)
hold on
loglog(n_e,tau_ion_constT(:,3),'r','LineWidth',3)
hold on
loglog(n_e,tau_ion_constT(:,4),'k','LineWidth',3)
hold on

xlabel('Density [m^-3]');
ylabel('Tau [s]');
title('Tau ionization const T, Tau recombination (dashed lines)')
% legend('0.026 [eV]','10 [eV]','100 [eV]','700 [eV]','0.026 [eV]','10 [eV]','100 [eV]','location','best')

% figure
loglog(n_e,tau_rec_constT(:,1),'LineWidth',3,'linestyle','--','color','b')
hold on
loglog(n_e,tau_rec_constT(:,2),'LineWidth',3,'linestyle','--','color','g')
hold on
loglog(n_e,tau_rec_constT(:,3),'LineWidth',3,'linestyle','--','color','r')
hold on
loglog(n_e,tau_rec_constT(:,4),'LineWidth',3,'linestyle','--','color','k')
legend('0.026 [eV]','10 [eV]','100 [eV]','700 [eV]','0.026 [eV]','10 [eV]','100 [eV]','700 [eV]','location','best')
% xlabel('Density [m^-3]');
% ylabel('Tau [s]');
% title('Tau recombination const T')
% legend('0.026 [eV]','10 [eV]','100 [eV]','700 [eV]','location','best')

figure
loglog(n_e,ris_ion_constT(:,1),'b','LineWidth',3)
hold on
loglog(n_e,ris_ion_constT(:,2),'g','LineWidth',3)
hold on
loglog(n_e,ris_ion_constT(:,3),'r','LineWidth',3)
hold on
loglog(n_e,ris_ion_constT(:,4),'k','LineWidth',3)
hold on
xlabel('density [m^-3]');
ylabel('Rates [m^3/s]');
title('Ionization rates const T, Recombination rates const T (dashed lines)')
% legend('0.026 [eV]','10 [eV]','100 [eV]','700 [eV]','0.026 [eV]','10 [eV]','100 [eV]','700 [eV]','location','best')

% figure
loglog(n_e,ris_rec_constT(:,1),'LineWidth',3,'linestyle','--','color','b')
hold on
loglog(n_e,ris_rec_constT(:,2),'LineWidth',3,'linestyle','--','color','g')
hold on
loglog(n_e,ris_rec_constT(:,3),'LineWidth',3,'linestyle','--','color','r')
hold on
loglog(n_e,ris_rec_constT(:,4),'LineWidth',3,'linestyle','--','color','k')
legend('0.026 [eV]','10 [eV]','100 [eV]','700 [eV]','0.026 [eV]','10 [eV]','100 [eV]','700 [eV]','location','best')
% xlabel('density [m^-3]');
% ylabel('Rates [m^3/s]');
% title('Recombination rates const T')
% legend('0.026 [eV]','10 [eV]','100 [eV]','700 [eV]','location','best')