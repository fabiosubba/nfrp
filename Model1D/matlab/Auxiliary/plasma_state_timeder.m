function [dxdt,diagnostic] = plasma_state_timeder(x,plasma,tau,pot,VV,const,...
    ionization_rate,recombination_rate,diagnostic,ii)
% Evaluate the time derivative of the plasma state vector x
% x(1:end-1) are the densities of different plasma ion species
% x(end) is the internal plasma energy density

dxdt = NaN(size(x));
dxdt(1:end-1) = 0;

particle_density = sum(plasma.ion_density)+plasma.electron_density;
temperature = plasma.energy_density/(1.5*particle_density)/const.ev;
En_ion = 0;

for ix = 1:numel(dxdt)-1
    name = plasma.atomic_symbol{ix};
    A = plasma.atomic_number(ix);
    Z = plasma.charge_state(ix);
    
    diagnostic.ionizations(ix,ii) = 0;
    diagnostic.recombinations(ix,ii) = 0;
    diagnostic.ionization_power(ix,ii) = 0;
    diagnostic.surface_losses(ii) = 0;
    diagnostic.total_ion_power(ii) = 0;
    diagnostic.auxiliary_heating(ii) = 0;
    
    if Z<A
        %No ionizations if Z=A (i.e., fully ionized atom)
        
        ionizations = EvalRate(plasma.electron_density,temperature,...
            A,Z,ionization_rate)*...
            plasma.electron_density*plasma.ion_density(ix);
        % Ionizations from Z to Z+1
        
        dxdt(ix) = dxdt(ix)-ionizations;
        dxdt(ix+1) = dxdt(ix+1)+ionizations;
        % Ionizations give two contributions to the particle evolution:
        % To the current ion as a sink, to the next ionization level as a
        % source
        
        En_ion = En_ion+ionizations*13.6*const.ev;
        %Sum up all the power density [W/m^3] due to ionizations for all
        %the ionizing species
        
        %         if mod(ii,0) == 0
        
        diagnostic.ionizations(ix,ii) = diagnostic.ionizations(ix,ii) + ionizations;
        %       diagnostic.recombinations(ix,ii) = 0;
        diagnostic.ionization_power(ix,ii) = En_ion;
        
        %         end
        
    end
    
    if Z>0
        %No recombinations if Z=0 (i.e., neutral atom)
        
        recombinations = EvalRate(plasma.electron_density,temperature,...
            A,Z,recombination_rate)*...
            plasma.electron_density*plasma.ion_density(ix);
        % Recombinations from Z to Z-1
        
        dxdt(ix) = dxdt(ix)-recombinations;
        dxdt(ix-1) = dxdt(ix-1)+recombinations;
        % Recombinations give two contributions to the particle evolution:
        % To the current ion as a sink, to the previous ionization level as a
        % source
        
        %         if mod(ii,6400) == 0
        %       diagnostic.ionizations(ix,ii) = 0;
        diagnostic.recombinations(ix,ii) = diagnostic.recombinations(ix,ii) + recombinations;
    end
    
end

% end
% dxdt(:) = 0;
dxdt(end) = - En_ion - plasma.energy_density/tau + pot/VV;
diagnostic.surface_losses(ii) = plasma.energy_density/tau;
diagnostic.total_ion_power(ii) = En_ion;
diagnostic.auxiliary_heating(ii) = pot/VV;

end