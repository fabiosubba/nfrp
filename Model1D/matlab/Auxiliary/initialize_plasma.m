function plasma = initialize_plasma(species,ne_0,T_0,total_density,const)
% Initialize the plasma

assert(numel(species)>0,'Cannot create an empty plasma');
assert(species{1}=='H','The first specified plasma species should be hydrogen');
% Sanity checkes

number_of_species = numel(species);

number_ions = 0;
for isp = 1:number_of_species
    number_ions = number_ions + AtomicNumber(species{isp}) + 1;
end
% Count how many ionic species (consider neutrals as ions with charge state 0)

plasma.atomic_symbol = cell(number_ions,1);
plasma.atomic_number = NaN(number_ions,1);
plasma.charge_state = NaN(number_ions,1);
plasma.ion_density = NaN(number_ions,1);
plasma.electron_density = NaN;
plasma.temperature = NaN;
% Initialize plasma data to an empty structure

ispecies_end = 0;
for isp = 1:number_of_species
    smb = species{isp};
    
    A = AtomicNumber(smb);
    ispecies_start = ispecies_end+1;
    ispecies_end = ispecies_start+A;
    
    plasma.atomic_symbol(ispecies_start:ispecies_end) = {smb};
    plasma.atomic_number(ispecies_start:ispecies_end) = A;
    plasma.charge_state(ispecies_start:ispecies_end) = transpose(0:A);
    plasma.ion_density(ispecies_start:ispecies_end) = 0;
    % Assign plasma composition. The ion density is initially set to zero
    % for all ions
end
plasma.ion_density(1) = total_density - ne_0;
plasma.ion_density(2) = ne_0;
% Complete the density initialization. It is implicitly assumed that only H
% can be ionized at t=0, independently on the actual set of plasma
% composing species

plasma.electron_density = ne_0;
% Initialize the plasma electron density

tmp = sum(plasma.ion_density)+plasma.electron_density;
plasma.energy_density = 1.5*tmp*T_0*const.KB;
% Initialize the plasma total energy.

end