function [E_value] = cons_E(EE,tau,Win) 

    E_value = -EE/tau + Win;

end