function [DTr] = DTFusionRate(temperature)
% [DTr] = DTFusionRate(temperature)
%
% Evaluate DT fusion reaction rate as a function of the mixture
% temperature.
% Input: temperature (eV)
% Output: DTr (m3s-1)
%
% Data source:
% Caughlan, G. R. & Fowler, W. A.
% "Thermonuclear reaction rates V"
% Atomic Data and Nuclear Data Tables, Elsevier BV, 1988, 40, 283-334
% DOI: 10.1016/0092-640x(88)90009-5

ph = PhysicalConstants();

temperatureK = temperature*ph.ev/ph.KB;
% Convert temperature to Kelvin

if temperatureK < 1e6
    DTr = 0;
    return
end
% If temperature is too low (<86 eV), set the reaction rate to zero

if 1e10 < temperatureK
    error('Data for temperature > 862 keV are not available!');
end
% No data for temperature > 862 keV. Issue an error message.

cgs2mks = 1e-6;
% Convertion factor between different metric systems

T9 = temperatureK/1e9;
% Convert the temperature to internal units. 
% For some misterious reason this is how the interpolation parameter are provided. 

T913 = T9.^(1/3);
T923 = T9.^(2/3);
T943 = T9.^(4/3);
T953 = T9.^(5/3);
% Set internal coefficients

DTr = 8.09E10./T923 .* exp(-4.524./T913 -(T9./0.120).^2) .* ...
    (1 + 0.092.*T913+1.80.*T923+1.16.*T9+10.52.*T943+17.24.*T953) + ...
    8.73E08./T923.*exp(-0.523./T9);
% Evaluate reaction rates. This result is not in the standard mks units

DTr = DTr ./ ph.NA;
DTr = DTr .* cgs2mks;
% Convert to mks

end
  

