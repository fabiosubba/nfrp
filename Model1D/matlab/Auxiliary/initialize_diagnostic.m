function diagnostic = initialize_diagnostic(species,ntime,time,plasma,T_0)

assert(numel(species)>0,'Cannot create an empty plasma');
assert(species{1}=='H','The first specified plasma species should be hydrogen');
% Sanity checkes

number_of_species = numel(species);

number_ions = 0;
for isp = 1:number_of_species
    number_ions = number_ions + AtomicNumber(species{isp}) + 1;
end
% Count how many ionic species (consider neutrals as ions with charge state 0)

diagnostic.ion_density = NaN(number_ions,ntime);
diagnostic.electron_density = NaN(ntime,1);
diagnostic.temperature = NaN(ntime,1);
diagnostic.energy_density = NaN(ntime,1);
diagnostic.time = NaN(ntime,1);
diagnostic.ionizations = NaN(number_ions,ntime);
diagnostic.recombinations = NaN(number_ions,ntime);
diagnostic.ionization_power = NaN(number_ions,ntime);
diagnostic.surface_losses = NaN(ntime,1);
diagnostic.total_ion_power = NaN(ntime,1);
diagnostic.auxiliary_heating = NaN(ntime,1);

diagnostic.ion_density(1,1) = plasma.ion_density(1,1);
diagnostic.electron_density(1,1) = plasma.electron_density(1,1);
diagnostic.temperature(1,1) = T_0 ;
diagnostic.energy_density(1,1) = plasma.energy_density(1,1);
diagnostic.time(1,1) = time(1);

end