close all; clear all; clc

% Specify the folder where the files live.
myFolder = 'C:\Users\tanzi\OneDrive\Documenti\politecnico\NFRP COLLAB\collaborazione_old11\Model1D\matlab\14-Oct-2022-230641';

% Check to make sure that folder actually exists.  Warn user if it doesn't.
if ~isfolder(myFolder)

    errorMessage = sprintf('Error: The following folder does not exist:\n%s\nPlease specify a new folder.', myFolder);
    uiwait(warndlg(errorMessage));
    myFolder = uigetdir(); % Ask for a new one.
    if myFolder == 0
         % User clicked Cancel
         return;
    end
end

% Get a list of all files in the folder with the desired file name pattern.
filePattern = fullfile(myFolder,'**/*.mat'); % Change to whatever pattern you need.
theFiles = dir(filePattern);

% Plot data
figure
for k = 1 : length(theFiles)

    baseFileName = theFiles(k).name;
    fullFileName = fullfile(theFiles(k).folder, baseFileName);
    dataToPlot = load(fullFileName);

    % Specify the data you want to plot on the y axe, i.e. "data.diagnostic.ion_density"
    plot(dataToPlot.diagnostic.time,dataToPlot.diagnostic.temperature/1e3,'LineWidth',2.5,'Color','r')
    hold on

end
xlabel('Time [s]')
ylabel('T [keV]')