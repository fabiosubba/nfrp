function [rat] = EvalRate(density,temperature,A,ionization_stage,rate)
% Evaluate the reaction rate.
% Need to specify the electron density and temperature, the selected
% chemical element and ionization stage, and the table with the rate data

ln = log(density);
lt = log(temperature);
% Get logarithmic values of density and temperature, needed to read the
% rate table

lr = rate(A).lograt(lt,ln,ionization_stage);
% rate(A).lograt is actually a gridded interpolant which returns the
% desired reaction rate

rat = exp(lr);

end