function [Bf] = B(r,~)

I = 1e6;
ui = [0 0 1];
P0 = [0 0 0];

Bf = B_Straight_Wire(I,ui,P0,r);
end

