close; clc; clear;
ph = PhysicalConstants();
% Set the environment

q = ph.ev;
m = ph.mp*2;
% Particle charge and mass

Ene0 = 1e3;
Ene_par = Ene0/3;
Ene_prp = Ene0 - Ene_par;
% Particle energy. ALso, split between parallel and
% perpendicular contributions

speed = sqrt(2*Ene0*ph.ev/3/m);
% Velocity along a single direction (for simplicity 
% It defines the velocity as equal along the three axis

v0 = speed*ones(3,1);
% Initial velocity

r0 = [1; 0; 0];
% Particle initial position

t0 = 0;
% Initial time (dummy argument for static fields)

B0 = B(r0,t0);
% Magnetic field at the initial position

tmp = q*cross(v0,B0);
url = tmp/norm(tmp);
% Unit vector pointing from the particle position 
% towards the gyration center

omg = q*norm(B0)/m;
% Larmor frequency


v_prp = sqrt(2*Ene_prp*ph.ev/m);
% Perpendicular speed

rl = v_prp/omg;
% Larmor radius

rC0 = r0 + rl*url;
% Initial gyration center position

mag_mom = (Ene_prp*ph.ev)/norm(B(rC0,t0));
% Magnetic moment

vpar_0 = sqrt(2*Ene_par*ph.ev/m); 
% Initial value of the parallel velocity

tau = 2*pi/omg;
% Gyration period

TAUMAX = 4e2*tau;
% Total simulation time: 400 gyrations

NT = 4e2;
% Number of required time steps

dt = TAUMAX/NT;
% Time step

tic;
[rC,vp,t] = trajectory_drift(rC0,vpar_0,q,m,mag_mom,@E,@B,NT,dt);
fprintf(1,'Elapsed time %13.6e\n',toc);

plot3(rC(1,:),rC(2,:),rC(3,:))
xlabel('X (m)');
ylabel('Y (m)');
zlabel('Z (m)');
set(gca,'dataaspectratio',[1 1 1])
set(gca,'xlim',[-1.01 1.01],'ylim',[-1.01 1.01],'zlim',[-1 2.0]);

