function Bf = B(r,~)

I = 1e5;
% Current intensity

P0 = [0; 0; 0];
% Loop center

uu = [0; 0; 1];
% Loop orientation

a = 2;
% Loop radius

Bf = Bloop(a,I,r,P0,uu);
% Field value

end