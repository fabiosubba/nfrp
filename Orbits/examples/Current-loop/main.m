% Plot a magnetic mirror field. The mirror is defined by
% two current-carrying circular rings, lying parallel to 
% the YZ plane. The centers are placed at X=-3, Y=Z=0, 
% and X=+3, Y=Z=0 (units are m). The rings radius is a=3, 
% and the current is I=1 MA.
close all; clear; clc;

I = 1e6;
% Current intensity

P0 = [0; 0; 0];
% Loop center

uu = [0; 0; 1];
% Loop orientation

a = 2;
% Loop radius

th = linspace(0,2*pi);
% Wire angular coordinate

figure;
hold on;
plot3(a*cos(th),a*sin(th),zeros(size(th)),'color','r','linewidth',2);
% Initialize the plot by putting the current wire

N = 4;
aa = linspace(-1.5,1.5,N);

dl = 1e-2;
% Define the lenght of a numerical integration step along 
% the field line. Units: m

nl = 1e4;
% Defines the total number of integration steps. Each 
% field line will be followed for a total length dl*nl

t0 = 0;
% Dummy value for time

for iax = 2:3
    x0 = aa(iax);
    for iay = 2:3
        y0 = aa(iay);
        r0 = [x0; y0; 0];
        tic;
        rl = FieldLine(r0,t0,dl,nl,@B);
        toc;
        plot3(rl(1,:),rl(2,:),rl(3,:),'color','b');
        % Add to the plot
    end
end

hq = quiver(0,0,0,0,0,2);
xlabel('X (m)');
ylabel('Y (m)');
zlabel('Z (m)');
set(gca,'DataAspectRatio',[1 1 1]);
set(gca,'xlim',[-8 8],'ylim',[-8 8],'zlim',[-5 5]);
set(hq,'linewidth',2,'color','k')
set(gca,'FontSize',14);
% Finalize the plot


