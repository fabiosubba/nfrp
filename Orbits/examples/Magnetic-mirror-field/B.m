function [Bf] = B(r,~)
% B(r,~)
% It is a function which builds the magnetic field created by
% a magnetic mirror, by summing up the fields of two planar
% current wires.
% Usage: Bf = B(r,t)
% Input: 
%   r - A 3x1 vector defining the location where the field has
%       to be computed.
%   t - A scalar defining the time at which the field has to be
%       computed. In this version it is actually a dummy argument
%       (i.e. it will not be used), because static fields will 
%       be produced
%   The user has to manually modify some parameters directly in 
%   in the function text. This is not a recommended programming
%   practice, but it is required here to allow compatibility with
%   auxiliary functions provided in the "Orbits" library. The 
%   parameters to be modified are:
%   a - A scalar defining the radius of the two current-carrying 
%       rings (assumed to equal to each other).
%   I - A scalar defining the current carried by the each one of 
%       the two rings.
%   uu - A 3x1 unit vector perpendicular to the ring planes. When
%        looked at from the uu direction, the current in each ring
%        is assumed to flow counter-clockwise. This means that the 
%        uu vector defines both the rings and current orientation.
%   P0 - A 3x1 vector defining the position of the first ring 
%        center
%   P1 - A 3x1 vector defining the position of the second ring
%        center
a = 3;
I = 1e6;
uu = [1; 0; 0];
% Defines the wires size, orientation, and current strength

P0 = [3; 0; 0];
P1 = [-3; 0; 0];
% Place in space the two wires


Bf0 = Bloop(a,I,r,P0,uu);
Bf1 = Bloop(a,I,r,P1,uu);
% Evaluates the fields of the two wires

Bf = Bf0+Bf1;
% Evaluates the total field

end

