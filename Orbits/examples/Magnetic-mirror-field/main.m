% Plot a magnetic mirror field. The mirror is defined by
% two current-carrying circular rings, lying parallel to 
% the YZ plane. The centers are placed at X=-3, Y=Z=0, 
% and X=+3, Y=Z=0 (units are m). The rings radius is a=3, 
% and the current is I=1 MA.
close all; clear; clc;

a = 3;
% Defines the wires radius

th = linspace(0,2*pi);
% Angular coordinate to plot the wires

x0 = -3*ones(size(th));
y0 = a*cos(th);
z0 = a*sin(th);
% First wire coordinates

x1 = 3*ones(size(th));
y1 = a*cos(th);
z1 = a*sin(th);
% Second wire coordinates

figure;
hold on;
plot3(x0,y0,z0,'color','r','linewidth',2);
plot3(x1,y1,z1,'color','r','linewidth',2);
xlabel('X (m)');
ylabel('Y (m)');
zlabel('Z (m)');
% Initialize the plot placing the twu current loops

thfield = linspace(0,2*pi,7);
% Define the position of a few magnetic field lines 
% for a nicer visualization

t0 = 0;
% Define the time at which the field is to be evalued
% (irrelevant for this script, because we assume
% stationary fields. Units: s

dl = 3e-3;
% Define the lenght of a numerical integration step along 
% the field line. Units: m

nl = 1e4;
% Defines the total number of integration steps. Each 
% field line will be followed for a total length dl*nl

RR = 2;
% Defines the distance between the magnetic lines and 
% the machine axis at the location where the lines run
% through the current wires

for il = 1:numel(thfield)-1
    r0 = [-3; RR*sin(thfield(il)); RR*cos(thfield(il))];
    % Defines the starting point of the field line
    
    rl = FieldLine(r0,t0,dl,nl,@B);
    % Integrate the spatial position along the selected line
    
    plot3(rl(1,:),rl(2,:),rl(3,:),'color','k','linewidth',2);
    % Add to the plot
    
end

set(gca,'DataAspectRatio',[1 1 1]);
% Make sure the final plot is not distorted


