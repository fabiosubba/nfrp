function [Bf] = B(r,~)
% Evaluates the magnetic field of a current wire running through
% point (0,0,0), oriented along the z axis. The current strength
% is 1 MA. The field is evaluated at position r. 

I = 1e6;
% Defines the current intensity (A)

ui = [0 0 1];
% Defines the current direction

P0 = [0 0 0];
% Place the current in space

Bf = B_Straight_Wire(I,ui,P0,r);
end

