close; clc; clear;
ph = PhysicalConstants();
% Set the environment

q = ph.ev;
% Set the particle charge

m = ph.mp*2;
% Set the particle mass (it is a D ion)

Ene0 = 1e3;
% Set the particle energy (in eV)

r0 = [1; 0; 0];
% Set the initial position

v0 = sqrt(2*Ene0*ph.ev/m/3)*ones(3,1);
% Set the initial velocity by equally splitting the energy
% among the three axis

t0 = 0;
% Set the time at which the axis should be evaluated
% (dummy value for static fields)

B0 = B(r0,t0);
omg = norm(q*B0)/m;
% Evaluates the Larmor frequency

tau = 2*pi/omg;
% Evaluates the gyration period

TAUMAX = 4e2*tau;
% Total simulation time: 400 gyrations

NT = 4e4;
% Total number of time steps. With such settings
% each Larmor gyration is divided in 100 time steps.

dt = TAUMAX/NT;
% Time step

tic;
[r,v,t] = trajectory(r0,v0,q,m,@E,@B,NT,dt);
fprintf(1,'Elapsed time %13.6e\n',toc);
% Evaluates the full trajectory and output the time
% needed for the computation.

X0 = [0 0];
Y0 = [0 0];
Z0 = [-1 6];
% Define the central wire 

figure;
hold on;
plot3(X0,Y0,Z0,'color','r','linewidth',2);
% Plot the central wire with a red, thick line

plot3(r(1,:),r(2,:),r(3,:),'color','b','linewidth',0.5)
% Plot the computed trajectory with a thinner, blue line

set(gca,'dataaspectratio',[1 1 1])
set(gca,'xlim',[-1.01 1.01],'ylim',[-1.01 1.01],'zlim',[-0.5 7.0]);
% Final touches

