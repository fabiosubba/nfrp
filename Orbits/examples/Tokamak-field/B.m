function [Bf] = B(r,~)
% B(r,~)
% It is a function which builds and returns a Tokamak-like magnetic
% field
% Usage: Bf = B(r,t)
% Input: 
%   r - A 3x1 vector defining the location where the field has
%       to be computed.
%   t - A scalar defining the time at which the field has to be
%       computed. In this version it is actually a dummy argument
%       (i.e. it will not be used), because static fields will 
%       be produced
%   The user has to manually modify some parameters directly in 
%   in the function text. This is not a recommended programming
%   practice, but it is required here to allow compatibility with
%   auxiliary functions provided in the "Orbits" library. The 
%   parameters to be modified are:
%   R - A scalar defining the tokamak major radius.
%   I - A scalar defining the plasma current.
%   ui - A 3x1 unit vector defining the direction of the current
%   flowing along the symmetry axis. This current generates the
%   toroidal field component.
%   P0 - A 3x1 vector defining the position of the tokamak center.
%   This acts both as the center of the plasma current and as a 
%   point through which the axial current flows.

I = 1e7;
% Axial current, which generates the magnetic field

ui = [0 0 1];
% Current direction

P0 = [0 0 0];
% Position of the tokamak center

Btor = B_Straight_Wire(I,ui,P0,r);
% Toroidal magnetic field

R = 3;
% Tokamak major radius

Ip = 2.e6;
% Plasma current strength

P0 = [0; 0; 0];
% Center of the plasma current loop

u0 = [0; 0; 1];
% Orientation of the plasma current loop

Bpol = Bloop(R,Ip,r,P0,u0);
% Poloidal magnetic field


Bf = Btor+Bpol;
% Total field

end

