% Produces and plot a Tokamak magnetic field.
% Toroidal component, produced by a Z-directed electric current crossing
% point [0 0 0], with strength I=10 MA.
% Poloidal component, produced by a current-carrying circular loop, with
% current intensity Ip=1 MA. The loop lies in the XY plane, centered at [0
% 0 0] and has radius R=3 m;
close all; clear; clc;

X0 = [0 0];
Y0 = [0 0];
Z0 = [-2 2];
% For plotting purposes, define the wire carrying the
% axial current

th = linspace(0,2*pi);
R = 3;
X1 = R*cos(th);
Y1 = R*sin(th);
Z1 = zeros(size(X1));
% For plotting purposes, define the loop carrying the
% plasma current

figure;
hold on;
plot3(X0,Y0,Z0,'linewidth',2,'color','r');
plot3(X1,Y1,Z1,'linewidth',2,'color','r');
% First plot the path of the currents creating the field

r0 = [2.5; 0; 0];
t0 = 0;
dl = 3e-3;
nl = 2e4;
rl = FieldLine(r0,t0,dl,nl,@B);
% Compute a magnetic field line starting at point r0

plot3(rl(1,:),rl(2,:),rl(3,:),'linewidth',2,'color','b');
% Add the field line

xlabel('R (m)');
ylabel('Y (m)');
zlabel('Z (m)');
set(gca,'DataAspectRatio',[1 1 1]);
% Plot finishing touches






