close; clc; clear;
ph = PhysicalConstants();
% Set the environment

q = ph.ev;
% Particle charge

m = ph.mp*2;
% Particle mass

Ene0 = 1e3;
Ene_par = Ene0/3;
Ene_prp = Ene0 - Ene_par;
% Particle energy. ALso, split between parallel and
% perpendicular contributions

speed = sqrt(2*Ene0*ph.ev/3/m);
% Velocity along a single direction (for simplicity 
% It defines the velocity as equal along the three axis

v0 = speed*ones(3,1);
% Initial velocity

r0 = [1; 0; 0];
% Particle initial velocity

t0 = 0;
% Initial time (dummy argument for static fields)

B0 = B(r0,t0);
% Magnetic field at the initial position

tmp = q*cross(v0,B0);
url = tmp/norm(tmp);
% Unit vector pointing from the particle position 
% towards the gyration center

omg = q*norm(B0)/m;
% Larmor frequency

v_prp = sqrt(4*Ene0*ph.ev/3/m);
% Perpendicular speed

rl = v_prp/omg;
% Larmor radius

rC0 = r0 + rl*url;
% Initial gyration center position

mag_mom = Ene_prp*ph.ev/norm(B(rC0,t0));
% Magnetic moment 

v0_par = speed;
% Initial parallel velocity

tau = 2*pi/omg;
% Evaluates the gyration period

TAUMAX = 8e1*tau;
% Total simulation time: 4000 gyrations

NT = 8e3;
% Total number of time steps. This means every 
% Larmor orbit is split in 100 steps

dt = TAUMAX/NT;
% Time step

tic;
[rC,~,~] = trajectory_drift(rC0,v0_par,q,m,mag_mom,@E,@B,NT/100,100*dt);
fprintf(1,'Elapsed time for drift computation %13.6e\n',toc);

tic;
[r,~,~] = trajectory(r0,v0,q,m,@E,@B,2*NT,dt/2);
fprintf(1,'Elapsed time for full computation %13.6e\n',toc);

figure;
hold on;
plot3(rC(1,:),rC(2,:),rC(3,:),'r');
plot3(r(1,:),r(2,:),r(3,:),'b');
set(gca,'dataaspectratio',[1 1 1]);
set(gca,'xlim',[-1.01 1.01],'ylim',[-1.01 1.01],'zlim',[-1 2.0]);

