LW = 2;
FS = 14;

r0 = [0; 0; 1];

BC = NaN(size(rC));
BB = NaN(1,numel(t));
for it = 1:numel(t)
    BC(:,it) = B(rC(:,it),t(it));
    BB(it) = norm(BC(:,it));
end
BB0 = norm(B(r0,0));

figure;
plot(t,rC(1,:),'linewidth',LW,'color','b');
xlabel('Time (s)');
ylabel('X (m)');
set(gca,'fontsize',FS);

figure;
plot(t,rC(2,:),'linewidth',LW,'color','b');
xlabel('Time (s)');
ylabel('Y (m)');
set(gca,'fontsize',FS);

figure;
plot(t,rC(3,:),'linewidth',LW,'color','b');
xlabel('Time (s)');
ylabel('Z (m)');
set(gca,'fontsize',FS);

figure;
plot(t,BB,'linewidth',LW,'color','b');
xlabel('Time (s)');
ylabel('B (T)');
set(gca,'fontsize',FS);
