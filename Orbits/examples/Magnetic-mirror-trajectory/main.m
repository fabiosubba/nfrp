close; clc; clear;
ph = PhysicalConstants();
% Set the environment

q = ph.ev;
m = ph.mp*2;
% Particle charge and mass

Ene0 = 1e3;
Ene_par = Ene0/3;
Ene_prp = Ene0 - Ene_par;
% Particle energy. ALso, split between parallel and
% perpendicular contributions

speed = sqrt(2*Ene0*ph.ev/3/m);
% Velocity along a single direction (for simplicity 
% It defines the velocity as equal along the three axis

v0 = speed*ones(3,1);
% Initial velocity

r0 = [0; 0; 1];
% Particle initial position

t0 = 0;
% Initial time (dummy argument for static fields)

B0 = B(r0,t0);
% Magnetic field at the initial position

tmp = q*cross(v0,B0);
url = tmp/norm(tmp);
% Unit vector pointing from the particle position 
% towards the gyration center

omg = q*norm(B0)/m;
% Larmor frequency


v_prp = 1.0*sqrt(2*Ene_prp*ph.ev/m);
% Perpendicular speed

rl = v_prp/omg;
% Larmor radius

rC0 = r0 + rl*url;
% Initial gyration center position

mag_mom = (Ene_prp*ph.ev)/norm(B(rC0,t0));
% Magnetic moment

vpar_0 = 1.5*sqrt(2*Ene_par*ph.ev/m); 
% Initial value of the parallel velocity

tau = 2*pi/omg;
% Gyration period

TAUMAX = 4e3*tau;
% Total simulation time: 400 gyrations

NT = 4e3;
% Number of required time steps

dt = TAUMAX/NT;
% Time step

BC0 = norm(B(rC0,t0)); 
B1 = BC0 + .5*m*vpar_0^2/mag_mom;
fprintf(1,'Starting field: %9.2e T\n',norm(B(rC0,t0)));
fprintf(1,'Reflection field: %9.2e T\n',B1);
keyboard;
tic;
[rC,vp,t] = trajectory_drift(rC0,vpar_0,q,m,mag_mom,@E,@B,NT,dt);
fprintf(1,'Elapsed time %13.6e\n',toc);


nth = 1e3;
th = linspace(0,2*pi,nth);
rl = [-3*ones(size(th)); 3*cos(th); 3*sin(th)];
rr = [ 3*ones(size(th)); 3*cos(th); 3*sin(th)];
figure;
hold on;
plot3(rl(1,:),rl(2,:),rl(3,:),'color','r','linewidth',2);
plot3(rr(1,:),rr(2,:),rr(3,:),'color','r','linewidth',2);
plot3(rC(1,:),rC(2,:),rC(3,:),'color','b','linewidth',1);
xlabel('X (m)');
ylabel('Y (m)');
zlabel('Z (m)');
set(gca,'dataaspectratio',[1 1 1])
set(gca,'xlim',[-4 4],'ylim',[-4 4],'zlim',[-4 4]);

