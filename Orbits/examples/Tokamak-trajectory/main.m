% Produces and plot a Tokamak magnetic field.
% Toroidal component, produced by a Z-directed electric current crossing
% point [0 0 0], with strength I=10 MA.
% Poloidal component, produced by a current-carrying circular loop, with
% current intensity Ip=1 MA. The loop lies in the XY plane, centered at [0
% 0 0] and has radius R=3 m;
close all; clear; clc;
ph = SetPhysConst();

X0 = [0 0];
Y0 = [0 0];
Z0 = [-2 2];
% For plotting purposes, define the wire carrying the
% axial current

th = linspace(0,2*pi);
R = 3;
X1 = R*cos(th);
Y1 = R*sin(th);
Z1 = zeros(size(X1));
% For plotting purposes, define the loop carrying the
% plasma current

figure;
hold on;
plot3(X0,Y0,Z0,'linewidth',2,'color','r');
plot3(X1,Y1,Z1,'linewidth',2,'color','r');
% First plot the path of the currents creating the field

r0 = [3.5; 0; 0];
t0 = 0;
dl = 3e-3;
nl = 1.1e3;
rl = FieldLine(r0,t0,dl,nl,@B);
% Compute a magnetic field line starting at point r0

plot3(rl(1,:),rl(2,:),rl(3,:),'linewidth',2,'color','b');
% Add the field line

xlabel('R (m)');
ylabel('Y (m)');
zlabel('Z (m)');
set(gca,'DataAspectRatio',[1 1 1]);
% Plot finishing touches

xpol = [sqrt(rl(1,:).^2+rl(2,:).^2)];
zpol = [rl(3,:)];
figure;
plot(xpol,zpol,'linewidth',2,'color','r');
xlabel('R (m)');
ylabel('Z (m)');
set(gca,'xlim',[2.6 3.6]);
% Plot the trajectory of a field line projected in the poloidal plane

Bf = NaN(size(xpol));
for ib = 1:numel(Bf)
    Bf(ib) = norm(B(rl(:,ib),0));
end
figure;
plot(xpol,Bf,'linewidth',2,'color','r');
xlabel('R (m)');
ylabel('B (T)');
% Plot the magnetic field strength along the trajectory

B0 = Bf(1);
Bmax = 1.689;
Eprp0 = 1e3*ph.ev;
mag_mom = Eprp0/B0;
Etot = mag_mom*Bmax;
Epar0 = Etot-Eprp0;
% Try to determine the particle trajectory extension in the poloidal plane

q = ph.ev;
m = ph.mp*2;
% Particle charge and mass

omg = q*B0/m;
% Larmor frequency

tau = 2*pi/omg;
% Gyration period

TAUMAX = 6e2*tau;
% Total simulation time. Express it as a finite number of gyrations

NT = 6.e2;
% Number of required time steps

dt = TAUMAX/NT;
% Time step

vpar_0 = -sqrt(2*Epar0/m);

tic;
[rC,vp,t] = trajectory_drift(r0,vpar_0,q,m,mag_mom,@E,@B,NT,dt);
fprintf(1,'Elapsed time %13.6e\n',toc);

figure
plot3(rC(1,:),rC(2,:),rC(3,:),'linewidth',2,'color','b');
xlabel('R (m)');
ylabel('Y (m)');
zlabel('Z (m)');
% Plot trajectory in 3D

Xpol = [sqrt(rC(1,:).^2+rC(2,:).^2)];
Zpol = [rC(3,:)];

figure;
hold on;
plot(xpol,zpol,'linewidth',2,'color','r');
plot(Xpol,Zpol,'linewidth',2,'color','b');
xlabel('R (m)');
ylabel('Z (m)');
set(gca,'xlim',[2.6 3.6]);
% Plot the trajectory of a field line projected in the poloidal plane

figure;
plot(Xpol,vp);
xlabel('R (m)');
ylabel('V_{//} (m/s)');
% Plot parallel velocity

Bff = NaN(size(Xpol));
for ib = 1:numel(Bff)
    Bff(ib) = norm(B(rC(:,ib),0));
end
EE = mag_mom*Bff+m/2*vp.^2;

figure;
plot(Xpol,EE);
xlabel('R (m)');
ylabel('Energy estimate (J)');
% Energy conservation check 1

figure;
plot(t,EE);
xlabel('Time (s)');
ylabel('Energy estimate (J)');
% Energy conservation check 2




