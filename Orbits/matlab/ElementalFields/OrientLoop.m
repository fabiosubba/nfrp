function [M,M1] = OrientLoop(ui)

EPS = 1e-5;

x=ui(1);
y=ui(2);
z=ui(3);

if z>(1-EPS)
    M = eye(3);
    M1 = M;
elseif z<(-1+eps)
    M = eye(3);
    M1 = M;
else
    zz = sqrt(1-z^2);
sa = x/zz;
ca = -y/zz;
sb = zz;
cb = z;

M = [ca -sa*cb  sa*sb;
     sa  ca*cb -ca*sb;
      0     sb     cb];
  
M1 = inv(M); 
end

end

