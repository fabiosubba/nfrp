function [Bl] = Bloop(Ra,I,rr,P,uu)

[M1,M1inv] = OrientLoop(uu);
% Find the transformations to and from a "standard" frame where the 
% field of the "standard" wire will be evaluated

r = M1inv*(rr(:)-P(:));
% Change coordinate system to be consistent with the "standard" wire
% (centered at the origin, oriented along the z axis)


Y = norm(r(1:2));
Z = r(3);
% The input position was given in a fixed (x,y,z) frame. However,
% function BiotSavart below will evaluate the field assuming in
% a local (X,Y,Z) frame with X=0. This is the transformation 
% (x,y,z) --> (X,Y,Z)
N=1000;
ITEMAX = 100;
TOLL = 1e-4;
g0 = BiotSavart(Ra,I,N,Y,Z);
for it = 1:ITEMAX
    N=2*N;
    g1 = BiotSavart(Ra,I,N,Y,Z);
    Ah2 = 4 * (g0-g1)/3;
    if norm(Ah2) < norm(g0)*TOLL
        break;
    end
    g0 = g1;
end
Bl = g0;

if abs(Y)>TOLL   
    Bl(1) = Bl(2)*r(1)/Y;
    Bl(2) = Bl(2)*r(2)/Y;
else
    Bl(1)=0.;
    Bl(2)=0.;
end
% Transform back the field (BX,BY,BZ) --> (Bx, By, Bz)

Bl = M1*Bl;
% Rotate back to place the field in the final frame

end
