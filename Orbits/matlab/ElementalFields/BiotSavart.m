function [B] = BiotSavart(Ra,I,N,Y,Z)

u0 = 1.256637061435917e-06;


phi=-pi/2:2*pi/(N-1):3*pi/2; % For describing a circle (coil)

Xc=Ra*cos(phi); % X-coordinates of the coil
Yc=Ra*sin(phi); % Y-coordinates of the coil

Rx = -0.5*(Xc+[Xc(2:N) Xc(1)]);
Ry = Y-0.5*(Yc+[Yc(2:N) Yc(1)]);
Rz = Z*ones(size(Rx));
% Vector from the current element to the position where the field
% is to be calculated

dlx = [Xc(2:N) Xc(1)]-Xc;
dly = [Yc(2:N) Yc(1)]-Yc;
% Current element vector


Xcross=dly.*Rz;
Ycross=-dlx.*Rz;
Zcross=(dlx.*Ry)-(dly.*Rx);
R=sqrt(Rx.^2+Ry.^2+Rz.^2);
% Cross product between the current element vector and the [Rx
% Ry Rz] vector


Bx1=(I*u0./(4*pi*(R.^3))).*Xcross;
By1=(I*u0./(4*pi*(R.^3))).*Ycross;
Bz1=(I*u0./(4*pi*(R.^3))).*Zcross;
% Biot-Savart law, element by element

BX = sum(Bx1);
BY = sum(By1);
BZ = sum(Bz1);
% Summation over the elemental contributions to obtain the total
% field

B = [BX; BY; BZ];

end


