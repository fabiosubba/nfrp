function [Bsw] = B_Straight_Wire(I,ui,P0,r)
%%
% Evaluates the magnetic field produced at position $\overline{r}$ by a
% straight current wire on total strength _I_, running through point
% $\overline{P}_{0}$ and directed along $\hat{u}_{i}$
%%

ph = PhysicalConstants();
%%
% Load a table of physical constants
%%

rr = (r(:)-P0(:));

rr0 = cross(ui(:),rr(:));


%%
% Get $\hat{u}_{r} = \hat{u}_{I} \times 
% (\overline{r} - \overline{P}_{0}) \times \hat{u}_{I}$.
% This is the unit vector pointing radially out from the corrent wire
% towards position $\overline{r}$.
%%

Bsw = ph.mu0*I / (2*pi*norm(rr0)^2) * rr0;
%%
% $\overline{B} = \frac{I}{2\pi\mu_{0}r}\hat{u}_{I} \times \hat{u}_{r}$
%%
% Make sure to return a _column_ vector.
%%

end

