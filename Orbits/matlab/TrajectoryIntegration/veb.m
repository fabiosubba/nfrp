function [v]=veb(E,B,r,t)
%%
% Evaluates the $\overline{E} \times \overline{B}$ velocity at position
% \overline{r} and time _t_
%%
Eloc = E(r,t);
Bloc = B(r,t);
%%
% Get the local values of $\overline{E}$ and $\overline{B}$
%%

v = cross(Eloc,Bloc) / (norm(Bloc).^2);
%%
% Assign \overline{v}_{E \times B}
%%
end