function [r,v,t] = trajectory(r0,v0,q,m,E,B,nt,dt)
%
% Integrates the trajectory of a charged particle of mass m and charge q in
% presence of electric and magnetic fields E(x,y,z) and B(x,y,z). 
%

r = zeros(3,nt+1);
v = zeros(3,nt+1);
t = zeros(1,nt+1);
% Store space for data

r(:,1) = r0(:);
v(:,1) = v0(:);
% Initialize the trajectory

for i = 1:nt
    rcur = r(:,i);
    vcur = v(:,i);
    tcur = t(i);
    % Extract data for RK4 step
    
    x = [rcur; vcur];
    k1 = lorentz(x,q,m,E,B,tcur);
    k2 = lorentz(x+.5*k1*dt,q,m,E,B,tcur+.5*dt);
    k3 = lorentz(x+.5*k2*dt,q,m,E,B,tcur+.5*dt);
    k4 = lorentz(x+k3*dt,q,m,E,B,tcur+dt);
    xn = x+((k1+2*k2+2*k3+k4)/6)*dt;
    % Perform a single RK4 step
    
    r(:,i+1) = xn(1:3);
    v(:,i+1) = xn(4:6);
    t(i+1) = tcur+dt;
    % Update position and time
        
end
end

