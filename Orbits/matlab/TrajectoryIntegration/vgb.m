function [v]=vgb(B,r,t,mu,q)
%%
% Evaluates the $\overline{\nabla} B$ velocity at position
% $\overline{r}$ and time _t_
%%
    gB = grad(@Bnorm,r,t);
    Bloc = B(r,t);
    %%
    % Evaluates $\overline{\nabla} B$ and $\overline{B}$ 
    %%
    
    v = mu / q * cross(Bloc,gB) / norm(Bloc)^2;
    %%
    % Evaluates the $\overline{\nabla} B$ as:
    %%
    %%
    % $\overline{v}_{\nabla B} = 
    % \frac{\mu}{q}
    % \frac{\overline B \times \overline{\nabla}{B}}{B^{2}}$
    %%
    %%
    % The magnetic monent $\mu$ and charge _q_ are inherited from the
    % calling environment.
    %%
    function [nB] = Bnorm(r,t)
        %%
        % Defines a function returning the strength of the B field
        %%
        nB = norm(B(r,t));
    end
end