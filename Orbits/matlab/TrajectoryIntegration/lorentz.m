function [xdot] = lorentz(x,q,m,E,B,t)
%
% Evaluates the velocity in phase space of a charged particle subject to
% Lorentz force
%
% Usage: [xdot] = lorentz(x,q,m,E,B,t)
% Input:
%   x: position in phase space of a charged particle. It is a vector of 6
%   components. x(1:3) set the particle position in the geometrical space,
%   x(4:6) set the particle position in the velocity space.
%
%   q: electric charge of the considered particle
%   m: mass of the considered particle
%   E: electric field. It must be described by an external fanction with
%   calling syntax E(r,t), with r = x(1:3)
%   B: magnetic field. It must be described by an external fanction with
%   calling syntax B(r,t), with r = x(1:3)
%   t: time
%
if ~iscolumn(x)
    x = transpose(x);
end
xdot = NaN(size(x));
r = x(1:3);
v = x(4:6);

EE = E(r,t);
BB = B(r,t);

xdot(1:3) = v(:);
xdot(4:6) = q/m * (cross(v(:),BB(:)) + EE(:));
end