function [xdot] = phase_velocity_drift(x,q,m,mu,E,B,t,options)
%%
% Evaluates the velocity in phase space of a charged particle according to
% drift theory
%
% Usage: [xdot] = phase_velocity_drift(x,q,m,mu,E,B,t)
%%
%%
% Input:
%%
% x: position in phase space. It is a vector of 4
% components. x(1:3) set the particle gyro-center position, 
% x(4) stores the parallel velocity.
%%
% q: particle electric charge.
%%
% m: particle mass.
%%
% mu: magnetic moment $\mu$ (a constant of the trajectory in drift
% theory)
%%
% E: electric field $\overline{E}$. It must be described by an external fanction with
% calling syntax E(r,t), with r = x(1:3)
%%
% B: magnetic field \overline{B}$. It must be described by an external fanction with
% calling syntax B(r,t), with r = x(1:3).
%%
% t: time

xdot = NaN(size(x));
r = x(1:3);
vp = x(4);
%%
% Split the its spatial part $\overline{r}=x(1:3)$ and its velocity part 
% $v_{\parallel}=x(4)$

BB = B(r,t);
bb = BB/norm(BB);

if options.EB
    v0=veb(E,B,r,t); 
else
    v0=0;
end
if options.GB 
    v1=vgb(B,r,t,mu,q); 
else
    v1=0; 
end
if options.CV
    v2=vkb(B,r,vp,t,m,q);
else
    v2=0;
end
v3=vp*bb;
%%
% Evaluates the four contributions to the gyro-center velocity:
% $\overline{v}_{E \times B}$, $\overline{v}_{\nabla B}$, 
% $\overline{v}_{k}$ and $\overline{v}_{\parallel}$

gB = grad(@Bnorm,r,t);
vv = v0(:)+v1(:)+v2(:)+v3(:);
acc_par = -mu*(sum(gB(:).*vv(:)))/(m*vp);
% Try enforcing energy conservation


% acc_par = -mu/m*derive(@Bnorm,r,t,bb);
% acc_par = sum(acc_par(:).*bb(:));
%%
% Evaluates the parallel acceleration as 
% $\frac{dv_{\parallel}}{dt} = -\frac{\mu}{m} \nabla_{\parallel} B$

xdot(1:3) = v0(:) + v1(:) + v2(:) + v3(:);
xdot(4) = acc_par;
%%
% Assemble the output vector with the phase-space velocity

%%
    function [nB] = Bnorm(r,t)
        % Defines a function returning the strength of the B field
        nB = norm(B(r,t));
    end

end