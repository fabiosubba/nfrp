function [v]=vkb(B,r,vp,t,m,q)
%%
% Evaluates the curvature drift celocity at position
% $\overline{r}$ and time _t_
%%

bloc = bunit(r,t);
kk = derive(@bunit,r,t,bloc);
%%
% Evaluates $\hat{b}$, the unit vector along $\overline{B}$
%%
%%
% Evaluates $\overline{k}$, the curvature vector of the magnetic field
% lines
%%

v = -m*vp.^2/q * cross(kk,bloc)/norm(B(r,t));
%%
% Evaluates the curvature drift as
%%
%%
% $\overline{v}_{k} = -\frac{m v_{\parallel}^{2}}{q} \times
% \frac{\overline{k} \times \hat{b}}{B}$
%%
%%
% The particle mass _m_ and charge _q_ are inherited from the calling
% environment
%%
    function [bb] = bunit(r,t)
        %%
        % Defines a function returning the unit vector along B
        %%
        bb = B(r,t) / norm(B(r,t));
    end
end