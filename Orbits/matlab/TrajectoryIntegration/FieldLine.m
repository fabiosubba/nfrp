function [r,l] = FieldLine(r0,t0,dl,nt,field)
%
% [r,l] = FieldLine(r0,t0,dl,nt,field)
%
% Follows a line of force of a 3D given field. A 
% Input:
%   r0 (vector): starting point of the trajectory
%   t0 (scalar): the time at which the trajectory is to be evaluated
%   (irrelevant if the field does not depend on time)
%   dl (scalar): the lenght of a single integration step
%   nt (scalar, integer): the number of required integration steps
%   field(vector field): the field to be followed. It must be an external
%   function with calling syntax "field(r,t)"
%
% Output:
%   r(3,nt): the computed field line trajectory (one position per column)
%   l: the total computed trajectory length
%

r = zeros(3,nt+1);
l = zeros(1,nt+1);
% Store space for data

r(:,1) = r0;
% Initialize the trajectory

for i = 1:nt
    rcur = r(:,i);
    lcur = l(i);
    % Extract data for RK4 step. 
    
    k1 = fnorm(field,rcur,t0);
    k2 = fnorm(field,rcur+.5*k1*dl,t0);
    k3 = fnorm(field,rcur+.5*k2*dl,t0);
    k4 = fnorm(field,rcur+k3*dl,t0);
    rn = rcur+((k1+2*k2+2*k3+k4)/6)*dl;
    % Perform a single RK4 step
    
    r(:,i+1) = rn(1:3);
    l(i+1) = lcur+dl;
    % Update position and length coordinate
        
end

function ff = fnorm(field,rr,tt)
    ff = field(rr,tt);
    ff = ff/norm(ff);
end
end