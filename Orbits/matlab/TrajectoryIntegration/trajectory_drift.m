function [rC,vp,t] = trajectory_drift(r0,v0,q,m,mu0,E,B,nt,dt,varargin)
%%
% Integrates the trajectory of a charged particle of mass m and charge q in
% presence of electric and magnetic fields E(x,y,z) and B(x,y,z), using the 
% drift approximation.
%
% Basic usage:
% [rC,vp,t] = trajectory_drift(r0,v0,q,m,mu0,E,B,nt,dt)
% r0 (INPUT)  - Initial position of the particle gyro-center
% v0 (INPUT)  - Initial parallel velocity
% q (INPUT)   - Particle charge
% m (INPUT)   - Particle mass
% mu0 (INPUT) - Particle magnetic moment
% E (INPUT)   - Electric field (it should point to an external function)
% B (INPUT)   - Magnetic field (it should point to an external function)
% nt (INPUT)  - Number of required time steps
% dt (INPUT)  - Time step amplitude
% rC (OUTPUT) - Gyro-center position calculated at different times. 
%               It is a matrix of size (nt,3). Column ith is the 
%               position at the ith time.
% vp (OUTPUT) - Vector of parallel velocities. One value for each 
%               time
% t (OUTPUT)  - Time vector
%
% Advanced usage:
% It isa possible to disable the evaluation of selected drift 
% components by specifying some optional input arguments. Such options
% should be specified as pairs of 'name','value' arguments. 
% Examples:
%  [rC,vp,t] = trajectory_drift(...,'GB','no') will disable 
%              evaluating the grad(B) drift
%  [rC,vp,t] = trajectory_drift(...,'VC','no') will disable 
%              evaluating the curvature drift
%  [rC,vp,t] = trajectory_drift(...,'EB','no') will disable 
%              evaluating the ExB drift
% The optional arguments **must** be specified after the list 
% mandatory ones. The user can select to disable two or even 
% all three drift contributions.
%%

options.GB = true;
options.CV = true;
options.EB = true;
% Assign options default values

for iArg = 1 : 2 : length( varargin )
    if strcmpi( varargin{iArg}, 'GradB' )
        tmp = varargin{ iArg + 1 };
        validatestring( tmp, { 'yes', 'no' },'trajectory_drift','GradB');
        if strcmpi( tmp, 'no' ); options.GB = false; end
    elseif( strcmpi( varargin{iArg}, 'Curvature' ))
        tmp = varargin{ iArg + 1 };
        validatestring( tmp, { 'yes', 'no' },'trajectory_drift','Curvature');
        if strcmpi( tmp, 'no' ); options.CV = false; end
    elseif( strcmpi( varargin{iArg}, 'ExB' ))
        tmp = varargin{ iArg + 1 };
        validatestring( tmp, { 'yes', 'no' },'trajectory_drift','ExB');
        if strcmpi( tmp, 'no' ); options.EB = false; end
    else
        error( 'Unknown property %s', varargin{iArg} );
    end
end
% Scan optional input parameters


t0 = 0;
%%
% Assign (arbitrarily) the initial time as $t_{0}=t=0$. 
%%


rC = zeros(3,nt+1);
vp = zeros(1,nt+1);
t = zeros(1,nt+1);
%%
% Store space for data. The phase space of this dynamical system comprises
% 4 coordinates. The first three correspond to the gyro-center position,
% the fourth is the parallel velocity. The other two velocity components
% are derived algebraically from the phase-space coordinates.
%%
 
rC(:,1) = r0;
vp(1) = v0;
t(1) = t0;
%%
% Initialize the trajectory. Here r0 must be the gyro-center initial
% position, and v0 the initial parallel velocity.
%%

for i = 1:nt
    rcur = rC(:,i);
    vcur = vp(i);
    tcur = t(i);
    % Extract data for RK4 step
    
    x = [rcur; vcur];
    k1 = phase_velocity_drift(x,q,m,mu0,E,B,tcur,options);
    k2 = phase_velocity_drift(x+.5*k1*dt,q,m,mu0,E,B,tcur+.5*dt,options); 
    k3 = phase_velocity_drift(x+.5*k2*dt,q,m,mu0,E,B,tcur+.5*dt,options); 
    k4 = phase_velocity_drift(x+k3*dt,q,m,mu0,E,B,tcur+dt,options); 
    xn = x+((k1+2*k2+2*k3+k4)/6)*dt;
    % Perform a single RK4 step
     
    t(i+1) = tcur+dt;
    rC(:,i+1) = xn(1:3);
    vp(i+1) = xn(4);
    % Update position and time. Try to enforce energy conservation
        
end


end

