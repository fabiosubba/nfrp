% runme
% Initialization script for the 'Orbits' library.
% It adds to the MATLAB path the location where all 
% the functions belonging to the 'Orbits' library are 
% stored. This script must *always* be run at the 
% beginning of a working session on the 'Orbits' 
% practical class.
close all; clear; clc;

fprintf(1,'Welcome to the Nuclear Fusion Reactor Physics \n');
fprintf(1,'practical classes!\n\n');
fprintf(1,'Initializing the matlab path \n');
fprintf(1,'for the ''Orbits'' library...\n\n');
addpath(genpath(pwd)); 
addpath(genpath(fullfile(pwd,'..','..','Common','matlab'))); 
fprintf(1,'Done.\n');
fprintf(1,'Now you can move MATLAB to \n');
fprintf(1,'your local folder and have fun\n');


