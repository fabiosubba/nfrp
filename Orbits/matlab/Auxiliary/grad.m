function [gf] = grad(f,r0,t)
%%
% Given the scalar function $f(\overline{r},t)$, estimate its gradient at
% position $r_{0}$ and time _t_
%

%%
assert(numel(r0)==3,'r0 should be a vector with 3 elements');
% Sanity check
%% 

hx = [1 0 0];
hy = [0 1 0];
hz = [0 0 1];
%%
% Prepare three unit vectors along directions _x_, _y_, and _z_.

gfx = derive(f,r0,t,hx);
gfy = derive(f,r0,t,hy);
gfz = derive(f,r0,t,hz);
%%
% Evaluates the three components of the gradient at position $r_{0}$ and
% time _t_.

gf = NaN(size(r0));
gf(1) = gfx;
gf(2) = gfy;
gf(3) = gfz;
%%
% Assign $\nabla (f)$. Make sure it is a vector with the same row/column
% orientation as $r_{0}$
end