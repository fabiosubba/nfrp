function [df,ok] = derive(f,r0,t,h)
% Given the function $f(\overline{r},t)$, evaluates the directional
% derivative along the direction $\overline{h}$.

%%
assert(numel(r0)==3,'r0 should be a vector with 3 elements');
assert(numel(h)==3,"h should be a vector with 3 elements");
% Sanity checks

%%
if size(r0,1)~=1
    r0 = transpose(r0);
end
if size(h,1)~=1
    h = transpose(h);
end
%%
% Make sure $\overline{r}_0$ and $\overline{h}$ are row vectors, at least internally to this
% function

%%
TOLL = 1e-3;
H0 = 1e-3;
ITEMAX = 10;
%% 
% Set the tolerance to 
% $TOLL = 10^{-3}$ (relative value) 
%% 
% Set the initial incremental step amplitude to $H0 = 10^{-3}$ m
%%
% Set the maximum number of iterations to $ITEMAX = 10$

hh = h / norm(h);
htmp = H0 * hh;
%%
% Set the first trial increment to 
%%
% $\overline{h}_{tmp} = H0 * \frac{\overline{h}}{h}$

g0 = IncrementalRatio(f,r0,htmp,t);
%%
% Evaluates 
%% 
% $\frac{\partial{\overline{f}}}{\partial{\overline{h}}} \approx 
% \frac{\overline{f}(\overline{r}_{0}+\overline{h})-
%       \overline{f}(\overline{r}_{0}-\overline{h})}{2h}$
%%
% This is known to be a $2^{nd}$-order approximation.

ok = false;
%%
% Initialize _ok_ to _false_. It will be reset to _true_ if the desired
% accuracy can be obtained within the allowed number of iterations. 
% Otherwise, the function still returns the most accurate available
% estimate, but _ok_ will be _false_.

for ite = 1:ITEMAX
%%  
% Start a loop to evaluate the derivative with the desired tolerance.
% To evaluate the accuracy obtained, we use a shortened version of the
% Richardson extrapolation approach. Given $G(\overline{h})$ the centered
% incremental ratio, it will be:
%%
% $G(\overline{h}) = 
% \frac{\partial{\overline{f}}}{\partial{\overline{h}}} + A \cdot h^{2} + \dots$
%%
% Then if $G_{0} = G(\overline{h})$ and $G_{1} = G(\overline{h})$
%%
% $A \cdot h^{2} = \frac{4}{3} \cdot (G_{0}-G_{1}) + \dots$
%%
% may be taken as an estimate of the error estimate

    htmp = htmp/2;
    g1 = IncrementalRatio(f,r0,htmp,t);
    %%
    % Shorten $\overline{h}_{tmp}$ to get the next derivative approximation
    % and evaluates the corresponding incremental ratio
    
    Ah2 = 4 * (g0-g1)/3;
    %%
    % Estimate the error $A \cdot h^{2}$
    
    if norm(Ah2) < norm(g0)*TOLL
        ok = true;
        break;
    end
    %%
    % If the error is sufficiently small, set _ok_ to _true_ and exit
     
    g0 = g1;
    %%
    % Update the derivative estimate to the latest calculated value 

end
df = g0;
%%
% Close the loop and assign the final derivative value.


    %%
    % Following is an internal function, which just evaluates the centered
    % incremental ratio needed to estimate a derivative.
    function [ddf] = IncrementalRatio(ff1,rr0,hh1,t)
        ddf = (ff1(rr0+hh1,t)-ff1(rr0-hh1,t)) / (2 * norm(hh1));
    end
end