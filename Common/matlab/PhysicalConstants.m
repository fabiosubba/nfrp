function [ const ] = PhysicalConstants(  )
%
% Output a data structure containing the values of the following physical
% constants
%
% me   - 9.1094e-31 kg - Electron mass
% mp   - 1.6726e-27 kg - Proton mass
% ev   - 1.6022e-19 C  - Electron charge
% eps0 - 8.8542e-12 Fm-1 - Permittivity of free space
% KB   - 1.3807e-23 JK-1 - Boltzmann Constant
% KB   - 1.3807e-16 ergK-1 - Boltzmann Constant (cgs system)
% KB_eV - 8.6173*1e-5 eV/K Boltzmann Constant 
% mu0  - 4*pi*1e-7  Hm-1 - Permeability of free space
% NA   - 6.0221e23  - Avogadro's number
%
const.me = 9.1094e-31;
const.mp = 1.6726e-27;
const.ev = 1.6022e-19;
const.eps0 = 8.8542e-12;
const.KB = 1.3807e-23;
const.KB_cgs = 1.3807e-16;
const.mu0 = 4*pi*1e-7;
const.NA = 6.0221e23;
const.KB_eV = 8.6173*1e-5;

end

